//
//  TileMap.h
//  SimpleRogue
//
//  Created by Annar Hilde on 15.11.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#ifndef SimpleRogue_TileMap_h
#define SimpleRogue_TileMap_h

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#include <array>

#include "Common.h"

#warning We need to implement functionality for updating the texture file. 
class TileMap : public sf::Drawable, public sf::Transformable
{
public:
    
    /*! Loads a texture that is used as the tileset, then creates an array of vertices and readies it for being rendered onscreen.
     \param tileset A path to the texture file for the tileset.
     \param tileSize The size of a single tile in (Width, Height).
     \param tiles An array consisting of integers whose numerical values correspond to a specific section in the texture file.
     \param width The size of the tile map that is to be generated, along the X-Axis.
     \param height The size of the tile that is to be generated, along the Y-Axis.
     */
    bool load(const std::string& tileset, sf::Vector2u tileSize, tileArray tiles, unsigned int width, unsigned int height);
    
    /*! Updates the tile map to correspond to the tiles parameter.
     \param tiles An array consisting of integers whose numerical values correspond to a specific section in the texture file.
     */
    void update(tileArray tiles);
    
private:
    /*! Once load has been succesfully called, we can start calling draw to render the tiles we have set up
     \param target 
     */
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
    {
        // apply the transform
        states.transform *= getTransform();
        
        // apply the tileset texture
        states.texture = &m_tileset;
        
        // draw the vertex array
        target.draw(m_vertices, states);
    }
    
    int m_width;
    int m_height;
    
    sf::Vector2u m_tileSize;
    sf::VertexArray m_vertices;
    sf::Texture m_tileset;
};

#endif
