
//
// Disclamer:
// ----------
//
// This code will work only if you selected window, graphics and audio.
//
// Note that the "Run Script" build phase will copy the required frameworks
// or dylibs to your application bundle so you can execute it on any OS X
// computer.
//
// Your resource files (images, sounds, fonts, ...) are also copied to your
// application bundle. To get the path to these resource, use the helper
// method resourcePath() from ResourcePath.hpp
//


#include <SFML/Graphics.hpp>

#include <iostream>

#include "TileMap.h"

#include "DungeonGenerator.h"
#include "BasicGenerator.h"
#include "CAutoGenerator.h"

#include "World.h"

#include <array>
#include <memory>

int main(int, char const**)
{
    /*
    int const GREYBRICKWALL1 = 1192;
    int const GREYBRICKWALL2 = 1193;
    int const GREYBRICKWALL3 = 1194;
    int const GREYBRICKWALL4 = 1195;
    int const GREYBRICKWALL5 = 1196;
    int const GREYBRICKWALL6 = 1197;
    int const STONEFLOOR1    = 809;
    int const STONEFLOOR2    = 810;
    int const STONEFLOOR3    = 811;
    int const STONEFLOOR4    = 812;
     */
    
    int zoomCoefficient = 32;
    
    // Create the main window
    sf::RenderWindow window(sf::VideoMode(800, 600), "SFML window");
    sf::View mainView;
    
    mainView.setSize(zoomCoefficient*_const::MAP_WIDTH, zoomCoefficient*_const::MAP_HEIGHT);
    mainView.setCenter((_const::TILE_SIZE*_const::MAP_WIDTH)/2, (_const::TILE_SIZE*_const::MAP_HEIGHT)/2);
    window.setView(mainView);

    TileMap map;
    
    World world;
    
    BasicGenerator basicGenerator;
    CAutoGenerator cellularAutomataGenerator;
    
    world.createDungeonFloor(cellularAutomataGenerator);
    
#warning We'll need to improve the path if we want this to work cross-platform.
#if TESTTILESET
    if (!map.load("tileset.png", sf::Vector2u(_const::TILE_SIZE, _const::TILE_SIZE), world.getArray(), _const::MAP_WIDTH, _const::MAP_HEIGHT)) {
        return -1;
    }
#else
    if (!map.load("/Users/Annar/Documents/Roguelike Graphics/Unfinalized ideas/Tilesets/Minimalistic/Test_GreyTileSet.png", sf::Vector2u(16, 16), world.getArray(), _const::MAP_WIDTH, _const::MAP_HEIGHT)) {
        return -1;
    }
#endif
    
    /* !-!-!-!-!-!-!-!-!-! GAME LOOP STARTS HERE !-!-!-!-!-!-!-! */
    while (window.isOpen())
    {
        // Process events
        sf::Event event;
        while (window.pollEvent(event))
        {   
            if(event.type == sf::Event::Closed)
            {
                window.close();
            }
            //Handle input
            else if(event.type == sf::Event::KeyPressed)
            {
                switch (event.key.code) {
                    //When the user presses the Escape key.
                    case sf::Keyboard::Escape:
                        window.close();
                        break;
                    //When the user presses down arrow key.
                    case sf::Keyboard::Down:
                        mainView.move(0, _const::TILE_SIZE);
                        window.setView(mainView);
                        break;
                     //When the user presses up arrow key.
                    case sf::Keyboard::Up:
                        mainView.move(0, -_const::TILE_SIZE);
                        window.setView(mainView);
                        break;
                    //When the user presses left arrow key.
                    case sf::Keyboard::Left:
                        mainView.move(-_const::TILE_SIZE, 0);
                        window.setView(mainView);
                        break;
                    //When the user presses right arrow key.
                    case sf::Keyboard::Right:
                        mainView.move(_const::TILE_SIZE, 0);
                        window.setView(mainView);
                        break;
                    case sf::Keyboard::R:
                        world.createDungeonFloor(cellularAutomataGenerator);
                        map.update(world.getArray());
                        break;
                    //When the user presses the Z key, zoom in.
                    case sf::Keyboard::Z:
                        if(zoomCoefficient > 2)
                        {
                            zoomCoefficient -= 2;
                            mainView.setSize(zoomCoefficient*_const::MAP_WIDTH, zoomCoefficient*_const::MAP_HEIGHT);
                            window.setView(mainView);
                        }
                        break;
                    //When the user presses the X key, zoom out.
                    case sf::Keyboard::X:
                        if(zoomCoefficient < _const::TILE_SIZE)
                        {
                            zoomCoefficient += 2;
                            mainView.setSize(zoomCoefficient*_const::MAP_WIDTH, zoomCoefficient*_const::MAP_HEIGHT);
                            window.setView(mainView);
                        }
                        break;
                    //When the user presses the C key, move the view to the center.
                    case sf::Keyboard::C:
                        mainView.setCenter((_const::TILE_SIZE*_const::MAP_WIDTH)/2, (_const::TILE_SIZE*_const::MAP_HEIGHT)/2);
                        window.setView(mainView);
                        break;
                    default:
                        break;
                }
            }
        }

        // Clear screen
        window.clear();
        
        window.draw(map);
        
        // Update the window
        window.display();
    }

    return EXIT_SUCCESS;
}
