//
//  MinimalDungeonGenerator.h
//  SimpleRogue
//
//  Created by Annar Hilde on 28.12.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#ifndef __SimpleRogue__MinimalDungeonGenerator__
#define __SimpleRogue__MinimalDungeonGenerator__

#include "DungeonGenerator.h"
#include "Common.h"


/*! This generator is very similiar to BasicGenerator, the only difference is that we don't create corridors between rooms as we generate rooms, instead we do this afterwards. This is intended to be done through Delaunay Triangulation(http://en.wikipedia.org/wiki/Delaunay_triangulation we can implement this through the basic algorithm given here 
 http://en.wikipedia.org/wiki/Bowyer–Watson_algorithm)
 */
class MinimalDungeonGenerator : public DungeonGenerator
{
public:
    void generateDungeon(tileArray& tiles);
    
private:
    static const int MAX_ROOMS  = 50;
    
    const int MIN_ROOM_SIZE_W   = 4;
    const int MAX_ROOM_SIZE_W   = 10;
    
    const int MIN_ROOM_SIZE_H   = 4;
    const int MAX_ROOM_SIZE_H   = 10;
    
    struct room {
        int x1;
        int y1;
        int x2;
        int y2;
        int centerX;
        int centerY;
    };
    
    /*! Creates a new room.
     \param xPos1 The X coordinate for the top-left corner
     \param yPos1 The Y coordinate for the top-left corner
     \param xPos2 The X coordinate for the bottom-right corner
     \param yPos2 The Y coordinate for the bottom-right corner
     \return A copy of the newly created room from the given parameters.
     */
    room createRoom(int xPos1, int yPos1, int xPos2, int yPos2);
    
    /*! Checks to see if two rooms are intersecting
     \param room1 The first room.
     \param room2 the second room.
     \return True if the rooms intersect, otherwise returns false.
     */
    bool roomIntersects(room room1, room room2);
    
};

#endif /* defined(__SimpleRogue__MinimalDungeonGenerator__) */
