//
//  RndNumberGenerator.cpp
//  SimpleRogue
//
//  Created by Annar Hilde on 05.12.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#include "RandomNumber.h"
#include "Common.h"
#include <chrono>


RandomNumber::RandomNumber()
{
    generator = std::make_unique<std::default_random_engine>(std::default_random_engine());
    generator->seed(std::chrono::system_clock::now().time_since_epoch().count());
}

int RandomNumber::generateRndInteger(int min, int max)
{
    std::uniform_int_distribution<int> distribution(min, max);
    return distribution(*generator);
}