#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=clang
CCC=clang++
CXX=clang++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=CLang-MacOSX
CND_DLIB_EXT=dylib
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/BSPGridGeneration.o \
	${OBJECTDIR}/BasicGenerator.o \
	${OBJECTDIR}/CAutoGenerator.o \
	${OBJECTDIR}/MinimalDungeonGenerator.o \
	${OBJECTDIR}/RandomNumber.o \
	${OBJECTDIR}/TileMap.o \
	${OBJECTDIR}/World.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-m32
CXXFLAGS=-m32

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L../SFML-2.2-osx-clang-universal/lib ../SFML-2.2-osx-clang-universal/lib/libsfml-audio.2.2.0.dylib ../SFML-2.2-osx-clang-universal/lib/libsfml-audio.dylib ../SFML-2.2-osx-clang-universal/lib/libsfml-graphics.2.2.0.dylib ../SFML-2.2-osx-clang-universal/lib/libsfml-graphics.dylib ../SFML-2.2-osx-clang-universal/lib/libsfml-network.2.2.0.dylib ../SFML-2.2-osx-clang-universal/lib/libsfml-network.dylib ../SFML-2.2-osx-clang-universal/lib/libsfml-system.2.2.0.dylib ../SFML-2.2-osx-clang-universal/lib/libsfml-system.dylib ../SFML-2.2-osx-clang-universal/lib/libsfml-window.2.2.0.dylib ../SFML-2.2-osx-clang-universal/lib/libsfml-window.dylib

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue: ../SFML-2.2-osx-clang-universal/lib/libsfml-audio.2.2.0.dylib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue: ../SFML-2.2-osx-clang-universal/lib/libsfml-audio.dylib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue: ../SFML-2.2-osx-clang-universal/lib/libsfml-graphics.2.2.0.dylib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue: ../SFML-2.2-osx-clang-universal/lib/libsfml-graphics.dylib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue: ../SFML-2.2-osx-clang-universal/lib/libsfml-network.2.2.0.dylib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue: ../SFML-2.2-osx-clang-universal/lib/libsfml-network.dylib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue: ../SFML-2.2-osx-clang-universal/lib/libsfml-system.2.2.0.dylib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue: ../SFML-2.2-osx-clang-universal/lib/libsfml-system.dylib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue: ../SFML-2.2-osx-clang-universal/lib/libsfml-window.2.2.0.dylib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue: ../SFML-2.2-osx-clang-universal/lib/libsfml-window.dylib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/BSPGridGeneration.o: BSPGridGeneration.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DSFML_STATIC -I../SFML-2.2-osx-clang-universal/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BSPGridGeneration.o BSPGridGeneration.cpp

${OBJECTDIR}/BasicGenerator.o: BasicGenerator.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DSFML_STATIC -I../SFML-2.2-osx-clang-universal/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BasicGenerator.o BasicGenerator.cpp

${OBJECTDIR}/CAutoGenerator.o: CAutoGenerator.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DSFML_STATIC -I../SFML-2.2-osx-clang-universal/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CAutoGenerator.o CAutoGenerator.cpp

${OBJECTDIR}/MinimalDungeonGenerator.o: MinimalDungeonGenerator.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DSFML_STATIC -I../SFML-2.2-osx-clang-universal/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MinimalDungeonGenerator.o MinimalDungeonGenerator.cpp

${OBJECTDIR}/RandomNumber.o: RandomNumber.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DSFML_STATIC -I../SFML-2.2-osx-clang-universal/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RandomNumber.o RandomNumber.cpp

${OBJECTDIR}/TileMap.o: TileMap.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DSFML_STATIC -I../SFML-2.2-osx-clang-universal/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/TileMap.o TileMap.cpp

${OBJECTDIR}/World.o: World.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DSFML_STATIC -I../SFML-2.2-osx-clang-universal/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/World.o World.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DSFML_STATIC -I../SFML-2.2-osx-clang-universal/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplerogue

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
