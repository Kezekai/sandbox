#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=CLang-MacOSX
CND_ARTIFACT_DIR_Debug=dist/Debug/CLang-MacOSX
CND_ARTIFACT_NAME_Debug=simplerogue
CND_ARTIFACT_PATH_Debug=dist/Debug/CLang-MacOSX/simplerogue
CND_PACKAGE_DIR_Debug=dist/Debug/CLang-MacOSX/package
CND_PACKAGE_NAME_Debug=simplerogue.tar
CND_PACKAGE_PATH_Debug=dist/Debug/CLang-MacOSX/package/simplerogue.tar
# Release configuration
CND_PLATFORM_Release=CLang-MacOSX
CND_ARTIFACT_DIR_Release=dist/Release/CLang-MacOSX
CND_ARTIFACT_NAME_Release=simplerogue
CND_ARTIFACT_PATH_Release=dist/Release/CLang-MacOSX/simplerogue
CND_PACKAGE_DIR_Release=dist/Release/CLang-MacOSX/package
CND_PACKAGE_NAME_Release=simplerogue.tar
CND_PACKAGE_PATH_Release=dist/Release/CLang-MacOSX/package/simplerogue.tar
# WindowsDebug configuration
CND_PLATFORM_WindowsDebug=MinGW_TDM-Windows
CND_ARTIFACT_DIR_WindowsDebug=dist/WindowsDebug/MinGW_TDM-Windows
CND_ARTIFACT_NAME_WindowsDebug=simplerogue
CND_ARTIFACT_PATH_WindowsDebug=dist/WindowsDebug/MinGW_TDM-Windows/simplerogue
CND_PACKAGE_DIR_WindowsDebug=dist/WindowsDebug/MinGW_TDM-Windows/package
CND_PACKAGE_NAME_WindowsDebug=simplerogue.tar
CND_PACKAGE_PATH_WindowsDebug=dist/WindowsDebug/MinGW_TDM-Windows/package/simplerogue.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
