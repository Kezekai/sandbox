//
//  CAutoGenerator.cpp
//  SimpleRogue
//
//  Created by Annar Hilde on 16.12.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#include "CAutoGenerator.h"
#include "RandomNumber.h"

#define DEBUG 0

#ifdef DEBUG
#include <iostream>
#endif

void CAutoGenerator::generateDungeon(tileArray &tiles)
{
    RandomNumber rng;
    cellArray cells;
    
    //Initialization step of the algorithm
    //Every cell has a percentage chance of being alive.
    //This is required since the rest of the algorithm depends on cells having different states.
    for(int x = 0; x < _const::MAP_WIDTH; ++x)
    {
        for(int y = 0; y < _const::MAP_HEIGHT; ++y)
        {            
            if(rng.generateRndInteger(1, 100) < CELL_ALIVE)
            {
                cells[x][y] = true;
            }
            else
            {
                cells[x][y] = false;
            }
        }
    }
    
    //Initialization is done
    //The next is to iterate over the entire dungeon floor and change tiles according to our ruleset
    for (int totalSteps = 0; totalSteps < MAX_STEPS; ++totalSteps) {
        doSimulationStep(cells);
    }
    
    //Floodfill.
    //Each flooded tile is stored here.
    floodArray flood;
    
    for (int x = 0; x < _const::MAP_WIDTH; ++x) {
        for (int y = 0; y < _const::MAP_HEIGHT; ++y) {
            //if the cell is a wall, the floodFill algorithm will not care about it.
            if(cells[x][y])
            {
                flood[x][y] = -1;
            }
            else
                flood[x][y] = 0;
        }
    }
    
    //If n equals the number an area is identified by in the flood array. Then a certain are has n-1 index
    //to refer to its size in this array. Size is just how many nodes are flooded.
    std::vector<int> sizeOfAreas;
    
    for (int x = 0; x < _const::MAP_WIDTH; ++x) {
        for(int y = 0; y < _const::MAP_HEIGHT; ++y)
        {
            if (flood[x][y] == 0) {
                floodFill(x, y, sizeOfAreas.size() + 1, flood, sizeOfAreas);
            }
        }
    }
    
    //Steps for the floodfill:
    //1. If target-color is equal to replacement-color return.
    //2. If the color is not equal to target-color, return.
    //3. Set the color of node to replacement-color.
    //4. Perform Flood-fill (one step to the west of node, target-color, replacement-color)
    //5. Perform Flood-fill (one step to the east of node, target-color, replacement-color)
    //6. Perform Flood-fill (one step to the north of node, target-color, replacement-color)
    //7. Perform Flood-fill (one step to the south of node, target-color, replacement-color)
    
#warning Temporary solution until we can manage to use different tilesets, so we can have caves that whilst using the same algorithm, they'll look different.
    for(int x = 0; x < _const::MAP_WIDTH; ++x)
    {
        for(int y = 0; y < _const::MAP_HEIGHT; ++y)
        {
            if(cells[x][y])
            {
                tiles[x][y] = _const::GREYBRICKWALL1;
            }
            else
            {
                tiles[x][y] = _const::STONEFLOOR1;
            }
        }
    }
}

int CAutoGenerator::countAliveNeighbours(int xPos, int yPos, cellArray& cells)
{
    int count = 0;
    
    for(int x = xPos-1; x <= xPos+1; ++x)
    {
        for(int y = yPos-1; y <= yPos+1; ++y)
        {
            if(x == xPos && y == yPos)
            {
                
            }
            else if(x < 0 || x == _const::MAP_WIDTH)
            {
#if DEBUG
                std::cout << "Counting around:(" << xPos << "," << yPos << ")--("
                          << x << "," << y << ")\n";
#endif
                count++;
            }
            else if(y < 0 || y == _const::MAP_HEIGHT)
            {
#if DEBUG
                std::cout << "Counting around:(" << xPos << "," << yPos << ")--("
                          << x << "," << y << ")\n";
#endif
                count++;
            }
            else if(cells[x][y])
            {
#if DEBUG
                std::cout << "Counting around:(" << xPos << "," << yPos << ")--("
                          << x << "," << y << ")\n";
#endif
                count++;
            }
        }
    }
    return count;
}
#warning we have changed the code here for now. Apparently when we using newState and returned it, the algorithm would falter after just 3 repeated generations. Even with using different objects everytime it would only compute part of the map. This seems like very odd behaviour that I don't understand at all. However, as it stands now, it seems to work. It IS unfortunate however that we have to copy the array every single step of the simulation. But we will probably not be doing much of other things during the generation of dungeon floors, so this might end up being okay.
#warning For the future we might want to not have to rely on copying it, and instead understand why we returning newState was having issues. 
void CAutoGenerator::doSimulationStep(cellArray &oldState)
{
    cellArray newState;
    
    for(int x = 0; x < _const::MAP_WIDTH; ++x)
    {
        for(int y = 0; y < _const::MAP_HEIGHT; ++y)
        {
            newState[x][y] = oldState[x][y];
        }
    }
    
    for(int x = 0; x < _const::MAP_WIDTH; ++x)
    {
        for(int y = 0; y < _const::MAP_HEIGHT; ++y)
        {
            int aliveCells = countAliveNeighbours(x, y, oldState);
            
#warning We may want to make the specific rules for cells be more modular than they currently are.
            if(newState[x][y])
            {
                if(aliveCells < DEATH_LIMIT)
                {
                    oldState[x][y] = false;
                }
                else
                {
                    oldState[x][y] = true;
                }
            }
            else if(aliveCells > BIRTH_LIMIT)
            {
                oldState[x][y] = true;
            }
        }
    }
    /*
#warning Is this using move semantics??
    return newState;
     */
}

int CAutoGenerator::countWalkableTiles(int xPos, int yPos, tileArray& tiles)
{
    int count = 0;
    
    for(int x = xPos - 1; x < xPos+1; ++x)
    {
        for(int y = yPos - 1; y < yPos+1; ++y)
        {
            if( (x >= 0 && x < _const::MAP_WIDTH) && (y >= 0 && y < _const::MAP_HEIGHT) )
            {
                /*
                if(tiles[x][y] >= _const::GREY_FLOOR_REG && tiles[x][y] <= _const::GREY_FLOOR_VAR004)
                {
                    count++;
                }
                */
                if(tiles[x][y] == false)
                {
                    count++;
                }
            }
        }
    }
    return count;
}

void CAutoGenerator::floodFill(int x, int y, int replacementColor, floodArray &arr, std::vector<int> &size)
{
    if (x < 0 || x >= _const::MAP_WIDTH || y < 0 || y >= _const::MAP_HEIGHT) {
        return;
    }
    else if(arr[x][y] == 0)
    {
        arr[x][y] = replacementColor;
        
        floodFill(x - 1, y, replacementColor, arr, size);
        floodFill(x, y - 1, replacementColor, arr, size);
        floodFill(x + 1, y, replacementColor, arr, size);
        floodFill(x, y + 1, replacementColor, arr, size);
    }
}