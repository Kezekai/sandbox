//
//  BasicGeneration.h
//  SimpleRogue
//
//  Created by Annar Hilde on 12.12.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#ifndef __SimpleRogue__BasicGeneration__
#define __SimpleRogue__BasicGeneration__

#include "DungeonGenerator.h"
#include "Common.h"

#warning Works well for smaller maps, but on larger a lot of corridors that go vertically become very long, and they tend to center around a line somewhere along the middle of the map, this creates dungeons that have little to no interconnectivity. Although all rooms can be traversed from any location within the dungeon.
/*! Generates a dungeon by placing rooms randomly(no intersecting rooms allowed) and for each room after
 the first has been generated, we pair up corridors with the newest room and the room that was made before
 it. This guarantees that the entire dungeon can be traversed. However, this may cause some very large
 corridors. For the player, this might be rather boring to travel.
 */
class BasicGenerator : public DungeonGenerator
{
public:
    void generateDungeon(tileArray& tiles);
    
private:
    static const int MAX_ROOMS  = 50;
    
    const int MIN_ROOM_SIZE_W   = 4;
    const int MAX_ROOM_SIZE_W   = 10;
    
    const int MIN_ROOM_SIZE_H   = 4;
    const int MAX_ROOM_SIZE_H   = 10;
        
    struct room {
        int x1;
        int y1;
        int x2;
        int y2;
        int centerX;
        int centerY;
    };
    
    /*! Creates a new room.
     \param xPos1 The X coordinate for the top-left corner
     \param yPos1 The Y coordinate for the top-left corner
     \param xPos2 The X coordinate for the bottom-right corner
     \param yPos2 The Y coordinate for the bottom-right corner
     \return A copy of the newly created room from the given parameters.
     */
    room createRoom(int xPos1, int yPos1, int xPos2, int yPos2);
    
    /*! Checks to see if two rooms are intersecting
     \param room1 The first room.
     \param room2 the second room.
     \return True if the rooms intersect, otherwise returns false.
     */
    bool roomIntersects(room room1, room room2);
    
    /*! Creates a new horizontal corridor.
     \param xPos1 The X coordinate for one end of the corridor.
     \param xPos2 A different X coordinate for the other end of the corridor.
     \param y The Y coordinate of the entire corridor.
     */
    void hCorridor(int xPos1, int xPos2, int y, tileArray& tiles);
    
    /*! Creates a new vertical corridor.
     \param yPos1 The Y coordinate for one end of the corridor.
     \param yPos2 A different Y coordinate for the other end of the corridor.
     \param x The X coordinate of the entire corridor.
     */
    void vCorridor(int yPos1, int yPos2, int x, tileArray& tiles);
};

#endif /* defined(__SimpleRogue__BasicGeneration__) */
