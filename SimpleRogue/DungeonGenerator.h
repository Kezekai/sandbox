//
//  DungeonGenerator.h
//  SimpleRogue
//
//  Created by Annar Hilde on 05.12.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#ifndef __SimpleRogue__DungeonGenerator__
#define __SimpleRogue__DungeonGenerator__

#include <memory>
#include "Common.h"

/*! Abstract class. Subclass from this one to create new dungeon generation algorithms.
 */
class DungeonGenerator
{
public:
    /*! Generates a dungeon by an algorithm implemented by the derived class.
     \param tiles Depending upon the implementation, anything stored in the array might be overridden.
     */
    virtual void generateDungeon(tileArray& tiles) = 0;
};

#endif /* defined(__SimpleRogue__DungeonGenerator__) */
