//
//  CAutoGenerator.h
//  SimpleRogue
//
//  Created by Annar Hilde on 16.12.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#ifndef __SimpleRogue__CAutoGenerator__
#define __SimpleRogue__CAutoGenerator__

#include <array>
#include <vector>
#include "DungeonGenerator.h"
#include "Common.h"

#warning The class as it is right now is implemented. However, when it tries to generate caves there are some parts, that will not have any connection. We need to make it guaranteed that all parts can be reached. Something along the lines of a floodfill? Either to identify if there are parts that can't reached and then make them reachable, or we regenerate the entire dungeon floor. This might be a costly approach though, unless we can guarantee that a valid floor will be made often. If using floodfill this article on wikipedia seems decent: http://en.wikipedia.org/wiki/Flood_fill

/*! Object encapsulating an algorithm to create a dungeon floor. This one uses a simple cellular automata to create floors that are cave-like. If an index in the cellArray is true, this means its an unwalkable tile. Otherwise it is walkable.
 */
class CAutoGenerator : public DungeonGenerator
{
public:
    void generateDungeon(tileArray& tiles);
    
private:
    /*! Just for the purpose of making things less verbose.
     */
    typedef std::array<std::array<bool, _const::MAP_HEIGHT>, _const::MAP_WIDTH> cellArray;
    
    /*! Array used for the flood fill part of the generateDungeon algorithm.
     */
    typedef std::array<std::array<int, _const::MAP_HEIGHT>, _const::MAP_WIDTH>  floodArray;
    
    /*! The percentage chance for any cell to become alive in the initialization step of the algorithm.
     */
    const int CELL_ALIVE = 40;
    
    /*! If a cell has less then this amount of alive cells surrounding it, it dies.
     */
    const int DEATH_LIMIT = 3;
        
    /*! If a cell has exactly this amount of alive cells surrounding it, it becomes alive.
     */
    const int BIRTH_LIMIT = 4;
    
#warning Do some testing with varying amounts of steps and see what works best, then include it in the comment right under this warning text.
    /*! The maximum amount of simulation steps we will do.
     */
    const int MAX_STEPS = 10;
    
    /*! Computes the number of alive neighbours any given cell has. Diagonals are counted as well, and out-of-border cells counts as alive.
     \param xPos The positon along the X-axis. Must be consistent with 0 <= x < _const::MAP_WIDTH.
     \param yPos The position along the Y-axis. Must be consistent with 0 <= y < _const::MAP_HEIGHT.
     */
    int countAliveNeighbours(int xPos, int yPos, cellArray& cells);
    
#warning The documentation and declaration of the function will remain. This is to serve as a reminder of what the function was originally intended to be. I want to see if we can't make this logic work properly.
    /*! Goes through one step of the simulation.
     \param oldState The current state of the simulation.
     \return Returns a cellArray that constitutes the next step in the simulation.
     */
//    cellArray doSimulationStep(cellArray& oldState);
    
    /*! Iterates forward one step in the simulation.
     \param oldState The current state of the simulation.
     */
    void doSimulationStep(cellArray& oldState);
    
#warning We will want to drag this function out into somewhere else 
    
    int countWalkableTiles(int xPos, int yPos, tileArray& tiles);
    
    void floodFill(int x, int y, int replacementColor, floodArray &arr, std::vector<int> &size);
};

#endif /* defined(__SimpleRogue__CAutoGenerator__) */
