//
//  RndNumberGenerator.h
//  SimpleRogue
//
//  Created by Annar Hilde on 05.12.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#ifndef __SimpleRogue__RndNumberGenerator__
#define __SimpleRogue__RndNumberGenerator__

#include <random>
#include <memory>

class RandomNumber
{
public:
    /*! The constructor seeds the generator with the computer clock.
     */
    RandomNumber();
    
    /*! Generates a random integer between (and including) the min and max parameters.
     \param min The lowest inclusive value.
     \param max The highest inclusive value.
     \return Returns an pseudo-random integer.
     */
    int generateRndInteger(int min, int max);
    
private:
    std::unique_ptr<std::default_random_engine> generator;
};

#endif /* defined(__SimpleRogue__RndNumberGenerator__) */
