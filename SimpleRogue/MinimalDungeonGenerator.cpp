//
//  MinimalDungeonGenerator.cpp
//  SimpleRogue
//
//  Created by Annar Hilde on 28.12.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#include "MinimalDungeonGenerator.h"
#include "RandomNumber.h"

#include <vector>
#include <array>

void MinimalDungeonGenerator::generateDungeon(tileArray &tiles)
{
    for(int x = 0; x < _const::MAP_WIDTH; ++x)
    {
        for(int y = 0; y < _const::MAP_HEIGHT; ++y)
        {
            tiles[x][y] = _const::GREYBRICKWALL1;
        }
    }
    
    std::vector<room> rooms;
    RandomNumber rng;
    
    for (int i = 0; i < MAX_ROOMS; ++i)
    {
        int width = rng.generateRndInteger(MIN_ROOM_SIZE_W, MAX_ROOM_SIZE_W);
        int height = rng.generateRndInteger(MIN_ROOM_SIZE_H, MAX_ROOM_SIZE_H);
        int xRoom = rng.generateRndInteger(1, _const::MAP_WIDTH - width - 1);
        int yRoom = rng.generateRndInteger(1, _const::MAP_HEIGHT - height - 1);
        
        room newRoom = createRoom(xRoom, yRoom, width, height);
        
        bool failed = false;
        
        for (auto itr = rooms.begin(); itr != rooms.end() && !failed; ++itr)
        {
            failed = roomIntersects(newRoom, *itr);
        }
        
        if(!failed)
        {
            for (int x = newRoom.x1; x <= newRoom.x2; ++x)
            {
                for(int y = newRoom.y1; y <= newRoom.y2; ++y)
                {
                    tiles[x][y] = _const::STONEFLOOR1;
                }
            }
            rooms.push_back(newRoom);
        }
    }
}   

MinimalDungeonGenerator::room MinimalDungeonGenerator::createRoom(int xPos1, int yPos1, int width, int height)
{
    room newRoom;
    newRoom.x1 = xPos1;
    newRoom.y1 = yPos1;
    newRoom.x2 = xPos1 + width;
    newRoom.y2 = yPos1 + height;
    newRoom.centerX = xPos1+((newRoom.x2-xPos1)/2);
    newRoom.centerY = yPos1+((newRoom.y2-yPos1)/2);
    return newRoom;
}

bool MinimalDungeonGenerator::roomIntersects(room room1, room room2)
{
    return (room1.x1-1 <= room2.x2+1) && (room1.x2+1 >= room2.x1-1)
    && (room1.y1-1 <= room2.y2+1) && (room1.y2+1 >= room2.y1-1);
}