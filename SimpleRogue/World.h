//
//  World.h
//  SimpleRogue
//
//  Created by Annar Hilde on 12.12.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#ifndef __SimpleRogue__World__
#define __SimpleRogue__World__

#include <array>
#include <vector>
#include <memory>

#include <SFML/Graphics/Sprite.hpp>
#include "Common.h"

/*! This class holds information about the world the player is in. The state of tiles,
    monsters on the tiles as well.
*/
class World
{
public:
    World();
    
    void createDungeonFloor(DungeonGenerator& dungeonGenerator);
    
    tileArray getArray();
    
private:
#warning All sprites that are on the dungeon floor will be stored here. However we may have issues with picking which ones to draw and not draw, depending on which ones are visible. But for now lets just keep it this way for simplicitys sake. If there are issues with performance we may want to be able to judge which sprites are relevant to draw, in a timely manner. (Sorting perhaps?)
    std::vector<sf::Sprite> spriteVec;
    
    /*! Two dimensional array. Use subscript operator to access elements. X is first index, Y is second.
     Represents tiles that are either walls, or floor.
     */
    tileArray walkingTiles;
};

#endif /* defined(__SimpleRogue__World__) */
