//
//  BSPGridGeneration.h
//  SimpleRogue
//
//  Created by Annar Hilde on 21.12.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#ifndef __SimpleRogue__BSPGridGeneration__
#define __SimpleRogue__BSPGridGeneration__

#include <iostream>
#include "DungeonGenerator.h"
#include "Common.h"

#warning This dungeon generation algorithm is supposed to use binary space partitioning to generate a grid-based dungeon layout.
#warning Fill in the documentation as well.
/*!
 */
class BSPGridGeneration : public DungeonGenerator
{
public:
    void generateDungeon(tileArray& tiles);
    
private:
    
};

#endif /* defined(__SimpleRogue__BSPGridGeneration__) */
