//
//  TileMap.cpp
//  SimpleRogue
//
//  Created by Annar Hilde on 15.11.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#include "TileMap.h"

/*! Function from the SFML tutorial. Loads a tileset that we will use to render a world. 
 \param tileset Path of the file for the texture of the tiles.
 \param tileSize Size of a specific tile given in number of pixels on the x and y axis respectively.
 \param tiles An array that consists of non-negative integers that signifies which of the tiles from        tileset we want to render.
 \param width The width of the tiles array.
 \param height The height of the tiles array.
*/
bool TileMap::load(const std::string& tileset, sf::Vector2u tileSize,
            tileArray tiles,unsigned int width, unsigned int height)
{
    m_width = width;
    m_height = height;
    m_tileSize = tileSize;
    // load the tileset texture
    if (!m_tileset.loadFromFile(tileset))
        return false;
    
    // resize the vertex array to fit the level size
    m_vertices.setPrimitiveType(sf::Quads);
    m_vertices.resize(m_width * m_height * 4);
    
    update(tiles);
    
    return true;
}

void TileMap::update(tileArray tiles)
{
    for (unsigned int i = 0; i < m_width; ++i)
        for (unsigned int j = 0; j < m_height; ++j)
        {
            // get the current tile number
            int tileNumber = tiles[i][j];
            
            // find its position in the tileset texture
            int tu = tileNumber % (m_tileset.getSize().x / m_tileSize.x);
            int tv = tileNumber / (m_tileset.getSize().x / m_tileSize.x);
            
            // get a pointer to the current tile's quad
            sf::Vertex* quad = &m_vertices[(i + j * m_width) * 4];
            
            // define its 4 corners
            quad[0].position = sf::Vector2f(i * m_tileSize.x, j * m_tileSize.y);
            quad[1].position = sf::Vector2f((i + 1) * m_tileSize.x, j * m_tileSize.y);
            quad[2].position = sf::Vector2f((i + 1) * m_tileSize.x, (j + 1) * m_tileSize.y);
            quad[3].position = sf::Vector2f(i * m_tileSize.x, (j + 1) * m_tileSize.y);
            
            // define its 4 texture coordinates
            quad[0].texCoords = sf::Vector2f(tu * m_tileSize.x, tv * m_tileSize.y);
            quad[1].texCoords = sf::Vector2f((tu + 1) * m_tileSize.x, tv * m_tileSize.y);
            quad[2].texCoords = sf::Vector2f((tu + 1) * m_tileSize.x, (tv + 1) * m_tileSize.y);
            quad[3].texCoords = sf::Vector2f(tu * m_tileSize.x, (tv + 1) * m_tileSize.y);
        }
}