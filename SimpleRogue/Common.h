//
//  Helper.h
//  SimpleRogue
//
//  Created by Annar Hilde on 12.12.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#ifndef __SimpleRogue__Helper__
#define __SimpleRogue__Helper__

#include <memory>

#define TESTTILESET 1

/* Original made by Stephan T. Lavavej, source can be found at: http://isocpp.org/files/papers/N3656.txt
 From revision 1 of C++14. Included here since we're using C++11.
*/
namespace std {
    template<class T> struct _Unique_if {
        typedef unique_ptr<T> _Single_object;
    };
    
    template<class T> struct _Unique_if<T[]> {
        typedef unique_ptr<T[]> _Unknown_bound;
    };
    
    template<class T, size_t N> struct _Unique_if<T[N]> {
        typedef void _Known_bound;
    };
    
    template<class T, class... Args>
    typename _Unique_if<T>::_Single_object
    make_unique(Args&&... args) {
        return unique_ptr<T>(new T(std::forward<Args>(args)...));
    }
    
    template<class T>
    typename _Unique_if<T>::_Unknown_bound
    make_unique(size_t n) {
        typedef typename remove_extent<T>::type U;
        return unique_ptr<T>(new U[n]());
    }
    
    template<class T, class... Args>
    typename _Unique_if<T>::_Known_bound
    make_unique(Args&&...) = delete;
}

namespace _const{
    const int MAP_WIDTH         = 125;
    const int MAP_HEIGHT        = 100;
    const int GREYBRICKWALL1    = 1192;
    const int STONEFLOOR1       = 809;
    const int GREY_FLOOR_REG    = 0;
    const int GREY_FLOOR_VAR001 = 1;
    const int GREY_FLOOR_VAR002 = 2;
    const int GREY_FLOOR_VAR003 = 3;
    const int GREY_FLOOR_VAR004 = 4;
    const int GREY_WALL_VAR001  = 5;
    const int GREY_WALL_VAR002  = 6;
    const int GREY_WALL_VAR003  = 7;
    const int GREY_WALL_NWCORN  = 8;
    const int GREY_WALL_NECORN  = 9;
    const int GREY_WALL_ESIDE   = 10;
    const int GREY_WALL_WSIDE   = 11;
    const int GREY_WALL_SECORN  = 12;
    const int GREY_WALL_SSIDE   = 13;
    const int GREY_WALL_SWCORN  = 14;
    const int TILE_SIZE         = 32;   //Size in pixels, never rectangular.
}

class DungeonGenerator;

//Adding documentation here so that we can see what the typedef is, apparently Xcode won't let us view the
//declaration of the typedef from other files without there being any documentation(even and empty one).
/*!*/
typedef std::array<std::array<int, _const::MAP_HEIGHT>, _const::MAP_WIDTH> tileArray;

#endif /* defined(__SimpleRogue__Helper__) */
