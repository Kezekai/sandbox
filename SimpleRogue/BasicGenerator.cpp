//
//  BasicGeneration.cpp
//  SimpleRogue
//
//  Created by Annar Hilde on 12.12.14.
//  Copyright (c) 2014 Annar Hilde. All rights reserved.
//

#include "BasicGenerator.h"
#include "RandomNumber.h"
#include <vector>
#include <memory>
#include <array>

void BasicGenerator::generateDungeon(tileArray& tiles)
{
    for(int x = 0; x < _const::MAP_WIDTH; ++x)
    {
        for(int y = 0; y < _const::MAP_HEIGHT; ++y)
        {
            tiles[x][y] = _const::GREYBRICKWALL1;
        }
    }
    
    std::vector<room> rooms;
    RandomNumber rng;
    
    for (int i = 0; i < MAX_ROOMS; ++i)
    {
        int width = rng.generateRndInteger(MIN_ROOM_SIZE_W, MAX_ROOM_SIZE_W);
        int height = rng.generateRndInteger(MIN_ROOM_SIZE_H, MAX_ROOM_SIZE_H);
        int xRoom = rng.generateRndInteger(1, _const::MAP_WIDTH - width - 1);
        int yRoom = rng.generateRndInteger(1, _const::MAP_HEIGHT - height - 1);
        
        room newRoom = createRoom(xRoom, yRoom, width, height);
        
        bool failed = false;
        
        for (auto itr = rooms.begin(); itr != rooms.end() && !failed; ++itr)
        {
            failed = roomIntersects(newRoom, *itr);
        }
        
        if(!failed)
        {
            for (int x = newRoom.x1; x <= newRoom.x2; ++x)
            {
                for(int y = newRoom.y1; y <= newRoom.y2; ++y)
                {
                    tiles[x][y] = _const::STONEFLOOR1;
                }
            }
            
            if(rooms.size() > 0)
            {
                //start with a horizontal or vertical corridor randomly
                if(rng.generateRndInteger(1, 2))
                {
                    hCorridor(rooms.front().centerX, newRoom.centerX, rooms.front().centerY, tiles);
                    vCorridor(rooms.front().centerY, newRoom.centerY, newRoom.centerX, tiles);
                }
                else
                {
                    vCorridor(rooms.front().centerY, newRoom.centerY, rooms.front().centerX, tiles);
                    hCorridor(rooms.front().centerX, newRoom.centerX, newRoom.centerY, tiles);
                }
            }
            
            rooms.push_back(newRoom);
        }
    }
}

BasicGenerator::room BasicGenerator::createRoom(int xPos1, int yPos1, int width, int height)
{
    room newRoom;
    newRoom.x1 = xPos1;
    newRoom.y1 = yPos1;
    newRoom.x2 = xPos1 + width;
    newRoom.y2 = yPos1 + height;
    newRoom.centerX = xPos1+((newRoom.x2-xPos1)/2);
    newRoom.centerY = yPos1+((newRoom.y2-yPos1)/2);
    return newRoom;
}

void BasicGenerator::hCorridor(int xPos1, int xPos2, int y, tileArray& tiles)
{
    if(xPos1 < xPos2)
    {
        for (int x = xPos1; x <= xPos2; ++x)
        {
            tiles[x][y] = _const::STONEFLOOR1;
        }
    }
    else
    {
        for (int x = xPos2; x <= xPos1; ++x) {
            tiles[x][y] = _const::STONEFLOOR1;
        }
    }
}

void BasicGenerator::vCorridor(int yPos1, int yPos2, int x, tileArray& tiles)
{
    if (yPos1 < yPos2)
    {
        for (int y = yPos1; y <= yPos2; ++y)
        {
            tiles[x][y] = _const::STONEFLOOR1;
        }
    }
    else
    {
        for(int y = yPos2; y <= yPos1; ++y)
        {
            tiles[x][y] = _const::STONEFLOOR1;
        }
    }
}

bool BasicGenerator::roomIntersects(room room1, room room2)
{
    return (room1.x1-1 <= room2.x2+1) && (room1.x2+1 >= room2.x1-1)
    && (room1.y1-1 <= room2.y2+1) && (room1.y2+1 >= room2.y1-1);
}